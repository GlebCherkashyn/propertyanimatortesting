//
//  ViewController.swift
//  PropertyAnimatorsTesting
//
//  Created by Gleb Cherkashyn on 4/25/19.
//  Copyright © 2019 Gleb Cherkashyn. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func loadView() {
        self.view = ViewControllerView()
    }
}

