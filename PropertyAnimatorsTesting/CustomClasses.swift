//
//  BaseView.swift
//  PropertyAnimatorsTesting
//
//  Created by Gleb Cherkashyn on 4/25/19.
//  Copyright © 2019 Gleb Cherkashyn. All rights reserved.
//

import UIKit

class BaseView: UIView {
    
    // MARK: - Initialization -
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: - Private functions -
    private func setup() {
        
        // Initialization of file owner
        let xibName = String(describing: type(of: self))
        let nib = UINib(nibName: xibName, bundle: nil)
        guard let view = nib.instantiate(withOwner: self, options: nil).first as? UIView else { return }
        
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
}

class InstantPanGestureRecognizer: UIPanGestureRecognizer {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        if (self.state == UIGestureRecognizer.State.began) { return }
        super.touchesBegan(touches, with: event)
        self.state = UIGestureRecognizer.State.began
    }
    
}
