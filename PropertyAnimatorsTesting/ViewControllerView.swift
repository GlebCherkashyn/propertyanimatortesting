//
//  ViewControllerView.swift
//  PropertyAnimatorsTesting
//
//  Created by Gleb Cherkashyn on 4/25/19.
//  Copyright © 2019 Gleb Cherkashyn. All rights reserved.
//

import UIKit

fileprivate enum ConstainerState {
    case expanded, collapsed
    
    mutating func toggleToOpposite() {
        self = opposite
    }
    
    var opposite: ConstainerState {
        return self == .expanded ? .collapsed : .expanded
    }
}

fileprivate enum Constants {
    static let tabBarContainerDefaultHeight: CGFloat = 60
    static let expandableContainerCollapsedHeight: CGFloat = 60
}

class ViewControllerView: BaseView {

    // MARK: - Outlets -
    @IBOutlet weak var tabBarViewHeight: NSLayoutConstraint!
    @IBOutlet weak var expandableContainerHeight: NSLayoutConstraint!
    
    // MARK: - Properties -
    private var containerState = ConstainerState.collapsed
    
    /// All of the currently running animators.
    private var runningAnimators: [UIViewPropertyAnimator] = []
    
    /// The progress of each animator. This array is parallel to the `runningAnimators` array.
    private var animationsProgress: [CGFloat] = []
    
    // MARK: - Actions -
    @IBAction func panGestureHandler(_ sender: InstantPanGestureRecognizer) {
        
        switch sender.state {
        case .began:
            
            // start required animations
            animateTransitionIfNeeded(to: containerState.opposite, duration: 1)
            
            // pause all animations to provide interactions in .changed case
            runningAnimators.forEach { $0.pauseAnimation() }
            
            // keep track of each animator's progress
            animationsProgress = runningAnimators.map { $0.fractionComplete }
            
        case .changed:
            
            // transitions calculations
            let translation = sender.translation(in: self)
            
            // the distance that user can pan with finger to expand the player
            let availableSpaceY = frame.height - Constants.tabBarContainerDefaultHeight - Constants.expandableContainerCollapsedHeight
            
            var fraction = -translation.y / availableSpaceY
            
            // adjust the fraction for the current player state and animations reversed state
            if containerState == .expanded { fraction *= -1 }
            if runningAnimators[0].isReversed { fraction *= -1 }
            
            // apply the new fractions
            for (index, animator) in runningAnimators.enumerated() {
                animator.fractionComplete = fraction + animationsProgress[index]
            }
            
        default:
            
            // variable setup
            let yVelocity = sender.velocity(in: self).y
            let shouldClose = yVelocity > 0
            
            // if there is no motion, continue all animations and exit early
            if yVelocity == 0 {
                runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
                break
            }
            
            // reverse the animations based on their current state and pan motion
            
             /// Found this way more "elegant"
//            let alreadyReversed = runningAnimators[0].isReversed
//
//            switch (playerState, shouldClose, alreadyReversed) {
//            case (.expanded, false, false),
//                 (.expanded, true, true),
//                 (.collapsed, true, false),
//                 (.collapsed, false, true):
//                self.runningAnimators.forEach { $0.isReversed = !$0.isReversed }
//            default:
//                ()
//            }
            
            // reverse the animations based on their current state and pan motion
            switch containerState {
            case .expanded:
                if !shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            case .collapsed:
                if shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if !shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            }
            
            // finish animations
            runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
        }
    }
    
    private func animateTransitionIfNeeded(to state: ConstainerState, duration: TimeInterval) {
        
        // ensure that the animators array is empty (which implies new animations need to be created)
        guard runningAnimators.isEmpty else { return }
        
        // containers animation
        
        let screenHeight = UIScreen.main.bounds.height
        let isCollapsed  = containerState == .collapsed
        let containerHeight = isCollapsed ? screenHeight : Constants.expandableContainerCollapsedHeight
        let tabBarHeight = isCollapsed ? 0 : Constants.tabBarContainerDefaultHeight
        
        let containersAnimator = UIViewPropertyAnimator(duration: duration,
                                                        curve: .easeOut)
        {
            self.tabBarViewHeight.constant = tabBarHeight
            self.expandableContainerHeight.constant = containerHeight
            self.layoutIfNeeded()
        }
        
        containersAnimator.addCompletion { position in
            
            switch position {
            case .start: self.containerState = state.opposite
            case .end: self.containerState = state
            case .current: ()
            @unknown default:
                fatalError("")
            }
            
            // manually reset the constraint positions to avoid existing bugs for property animators
            self.tabBarViewHeight.constant = tabBarHeight
            self.expandableContainerHeight.constant = containerHeight
            
            // cleaning up animators array
            self.runningAnimators.removeAll()
        }
        
        // starting defined animations
        containersAnimator.startAnimation()
        
        // keep track of all running animators
        runningAnimators.append(containersAnimator)
    }
}
